export default {
  srcDir: '.',
  distDir: 'dist',
  baseDir: 'lp',
  gzip: false,
  webp: false
}
