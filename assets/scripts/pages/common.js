import 'objectFitPolyfill'
import 'what-input'
import 'intersection-observer'
import picturefill from 'picturefill'
import _ from 'lodash'
import Rellax from 'rellax'
import Collapse from '../modules/_Collapse'
import Scroller from '../modules/_scroller'
import ScrollActive from '../modules/_scrollActive'
import ToggleConversionButton from '../modules/_ToggleConversionButton'

{
  const scroller = new Scroller('[data-scroll]')
  scroller.init()
}

// 375px以下でviewportを固定
{
  const viewportEl = document.querySelector('meta[name="viewport"]')
  const mediaQueryList = window.matchMedia('(min-width: 765px)')

  function onChange() {
    const viewportContent = mediaQueryList.matches
      ? 'width=device-width, initial-scale=1'
      : 'width=375'

    viewportEl.setAttribute('content', viewportContent)
  }

  mediaQueryList.addListener(onChange)
  onChange()
}

// picturefill stickyfill
{
  const mql = window.matchMedia('(min-width: 765px)')
  const fill = () => {
    picturefill()
    window.objectFitPolyfill()
  }
  mql.addListener(fill)
  fill()
}

//  Collapse
window.addEventListener('load', () => {
  _.forEach(document.querySelectorAll('[data-collapse-toggler]'), el => {
    const collapse = new Collapse(el, {
      hashNavigation: false
    })
    collapse.init()
  })
})

// scroll active
{
  const targets = [...document.querySelectorAll('[data-scroll-active]')]
  targets.forEach(target => {
    const scrollActive = new ScrollActive(target)
    scrollActive.init()

    const clientRectTop = target.getBoundingClientRect().top
    const clientHeihgt = target.clientHeight

    if (clientRectTop + clientHeihgt <= 0) {
      scrollActive.intersected(target)
    }
  })
}

// pallax
{
  const rellax = new Rellax('[data-rellax-speed]', {
    breakpoints: [767],
    center: true
  })
  rellax.refresh()
}

{
  class ModalToggle {
    constructor() {
      this.targetParent = document.querySelector('.product-modal')
      this.targets = document.querySelectorAll('[js-modal-target]')
      this.triggers = document.querySelectorAll('[js-modal-btn]')
      this.closeTrigger = document.querySelector('[js-modal-close]')
    }

    init() {
      this.triggerClick()
      this.modalClose()
    }

    triggerClick() {
      this.triggers.forEach(trigger => {
        trigger.addEventListener('click', () => {
          this.modalOpen(trigger.getAttribute('js-modal-btn'))
        })
      })
    }

    modalOpen(value) {
      this.targetParent.classList.add('is-active')
      const TARGET = document.getElementById(value)
      TARGET.classList.add('is-active')
    }

    modalClose() {
      this.closeTrigger.addEventListener('click', () => {
        this.targetParent.classList.remove('is-active')
        const CLOSE_TARGET = document.querySelector('.modal.is-active')
        CLOSE_TARGET.classList.remove('is-active')
      })
    }
  }

  const MODAL_TOGGLE = new ModalToggle()
  MODAL_TOGGLE.init()
}

// 燕を押した時の挙動SPのみ
{
  const mediaQueryList = window.matchMedia('(max-width: 765px)')

  function btnClickSwallow() {
    if (!mediaQueryList.matches) return
    const buttons = [...document.getElementsByClassName('c-liblary__button')]
    buttons.forEach(target => {
      target.addEventListener('click', e => {
        if (target.classList.contains('is-active')) {
          target.classList.remove('is-active')
        } else {
          target.classList.add('is-active')
        }
      })

      document.addEventListener('click', e => {
        if (
          !e.target.closest('.c-liblary__button') &&
          !e.target.closest('.c-liblary__card')
        ) {
          target.classList.remove('is-active')
        }
      })
    })
  }
  mediaQueryList.addListener(btnClickSwallow)
  btnClickSwallow()
}

{
  // コンバージョンボタンをfooterで消す

  const toggleConversionButton = new ToggleConversionButton()
  toggleConversionButton.init()
}
