import 'intersection-observer'
const option = {
  rootMargin: '0px',
  threshold: [0]
}

export default class {
  constructor() {
    this.trigger = document.getElementById('js-footer')
    this.target = document.getElementById('js-conversion-button')
  }

  init() {
    const observer = new IntersectionObserver(
      this.isTargetCross.bind(this),
      option
    )
    observer.observe(this.trigger)
  }

  isTargetCross(entries, observer) {
    entries.forEach(entry => {
      // 交差している場合はtrue
      if (entry.isIntersecting) {
        this.target.style.opacity = 0
        this.target.style.visibility = 'hidden'
      } else {
        this.target.style.opacity = 1
        this.target.style.visibility = 'visible'
      }
    })
  }
}
